package ru.vkandyba.tm.api.service;

import ru.vkandyba.tm.api.repository.IRepository;
import ru.vkandyba.tm.enumerated.Status;
import ru.vkandyba.tm.model.Project;
import ru.vkandyba.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService extends IRepository<Task> {

    Task findByName(String name);

    Task removeByName(String name);

    Task updateByIndex(Integer index, String name, String description);

    Task updateById(String id, String name, String description);

    Task startById(String id);

    Task startByIndex(Integer index);

    Task startByName(String name);

    Task finishById(String id);

    Task finishByIndex(Integer index);

    Task finishByName(String name);

    Task changeStatusById(String id, Status status);

    Task changeStatusByIndex(Integer index, Status status);

    Task changeStatusByName(String name, Status status);

    void create(String name);

    void create(String name, String description);

    void clear();

}
