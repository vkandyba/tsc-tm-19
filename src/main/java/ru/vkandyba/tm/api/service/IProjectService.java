package ru.vkandyba.tm.api.service;

import ru.vkandyba.tm.api.repository.IRepository;
import ru.vkandyba.tm.enumerated.Status;
import ru.vkandyba.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService extends IRepository<Project> {

    Project findByName(String name);

    Project removeByName(String name);

    Project updateByIndex(Integer index, String name, String description);

    Project updateById(String id, String name, String description);

    Project startById(String id);

    Project startByIndex(Integer index);

    Project startByName(String name);

    Project finishById(String id);

    Project finishByIndex(Integer index);

    Project finishByName(String name);

    Project changeStatusById(String id, Status status);

    Project changeStatusByIndex(Integer index, Status status);

    Project changeStatusByName(String name, Status status);

    void create(String name);

    void create(String name, String description);

    void clear();

}
