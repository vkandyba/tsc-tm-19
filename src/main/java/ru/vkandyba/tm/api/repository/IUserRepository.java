package ru.vkandyba.tm.api.repository;

import ru.vkandyba.tm.model.User;

import java.util.List;

public interface IUserRepository extends IRepository<User>{

    User findByLogin(String login);

    User removeUser(User user);

    User removeByLogin(String login);

}
