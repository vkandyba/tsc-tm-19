package ru.vkandyba.tm.api.repository;

import ru.vkandyba.tm.enumerated.Status;
import ru.vkandyba.tm.model.Project;
import ru.vkandyba.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository extends IRepository<Task>{

    Task findByName(String name);

    Task removeByName(String name);

    Task startById(String id);

    Task startByIndex(Integer index);

    Task startByName(String name);

    Task finishById(String id);

    Task finishByIndex(Integer index);

    Task finishByName(String name);

    Task changeStatusById(String id, Status status);

    Task changeStatusByIndex(Integer index, Status status);

    Task changeStatusByName(String name, Status status);

    Task bindTaskToProjectById(String projectId, String taskId);

    Task unbindTaskToProjectById(String projectId, String taskId);

    List<Task> findAllTaskByProjectId(String projectId);

    void removeAllTaskByProjectId(String projectId);

    void clear();

}
