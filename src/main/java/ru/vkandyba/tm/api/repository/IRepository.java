package ru.vkandyba.tm.api.repository;

import ru.vkandyba.tm.model.AbstractEntity;

import java.util.Comparator;
import java.util.List;

public interface IRepository<E extends AbstractEntity> {
    
    Boolean existsByIndex(Integer index) ;
    
    Boolean existsById(String id);
    
    E findById(String id);
    
    E findByIndex(Integer index);
    
    E removeById(String id);
    
    E removeByIndex(Integer index);
    
    E add(E entity);
    
    void remove(E entity);

    List<E> findAll();
    
    List<E> findAll(Comparator<E> comparator);

}
