package ru.vkandyba.tm.command.task;

import ru.vkandyba.tm.command.AbstractCommand;
import ru.vkandyba.tm.util.TerminalUtil;

public class TaskUnbindFromProjectByIdCommand extends AbstractCommand {

    @Override
    public String name() {
        return "unbind-task-from-project-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Unbind task from project by id...";
    }

    @Override
    public void execute() {
        System.out.println("Enter project id");
        final String projectId = TerminalUtil.nextLine();
        if (serviceLocator.getProjectService().findById(projectId) == null) {
            System.out.println("Incorrect values");
            return;
        }
        System.out.println("Enter task id");
        final String taskId = TerminalUtil.nextLine();
        if (serviceLocator.getTaskService().findById(taskId) == null) {
            System.out.println("Incorrect values");
            return;
        }
        serviceLocator.getProjectTaskService().unbindTaskToProjectById(projectId, taskId);
    }

}
